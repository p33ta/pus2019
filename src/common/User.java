package common;

import server.Db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class User {
    private int id;
    private String email;
    private String nick;

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getNick() {
        return nick;
    }

    public User(String email, String password, String nick) {
        this.id = -1;
        this.email = email;
        this.nick = nick;
        try {
            PreparedStatement st = Db.connection.prepareStatement("INSERT INTO users (email,password,nick) VALUES (?,?,?)");
            st.setString(1, email);
            st.setString(2, password);
            st.setString(3, nick);
            st.execute();
            ResultSet rs = st.getGeneratedKeys();
            this.id = rs.getInt(1);
        } catch(SQLException ex) {};
    }

    public static int getUsersCount() {
        int result = -1;
        try {
            Statement st = Db.connection.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM users");
            rs.next();
            result = rs.getInt(1);
        } catch(SQLException ex) {}
        return result;
    }
}