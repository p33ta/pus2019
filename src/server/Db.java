package server;

import java.io.IOException;
import java.sql.*;

public class Db {
    public static Connection connection = null;

    public Db(String dbDriver, String dbUrl) throws IOException, SQLException, ClassNotFoundException {
        if(connection == null) {
            Class.forName(dbDriver);
            connection = DriverManager.getConnection(dbUrl);
            Statement st = connection.createStatement();
            // st.execute("DROP TABLE users");
            st.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, email varchar(40), password varchar(40), nick varchar(20))");
        }
    }
}
