/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import common.User;
import server.Db;

/**
 * @author Mariusz
 */
public class Server implements Runnable {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    private static int port;
    private static JLabel nThreadsLabel, nRegisteredUsers;
    private Socket sock;
    private PrintWriter out;
    private String login = null;
    private HashSet<String> sendTo = new HashSet<>();
    private static HashSet<Server> serverPool = new HashSet<>();

    public static Db db;

    private Server(Socket sock) throws IOException {
        this.sock = sock;
    }

    public static void main(String[] args) throws IOException {
        ServerSocket ssock = null;
        try {
            Properties props = new Properties();
            props.load(new FileInputStream("Server.properties"));
            Server.db = new Db(props.getProperty("dbDriver"), props.getProperty("dbUrl"));
            port = Integer.parseInt(props.getProperty("port"));
            ssock = new ServerSocket(port);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "While binding port " + port + "\n" + e);
            System.exit(1);
        }

        JFrame mainWindow = new JFrame("Communicator server on port " + port);
        mainWindow.setSize(300, 100);
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container interior = mainWindow.getContentPane();
        interior.setLayout(new GridLayout(2, 2));
        interior.add(new JLabel("Registered users", JLabel.CENTER));
        nRegisteredUsers = new JLabel("0", JLabel.CENTER);
        interior.add(nRegisteredUsers);
        interior.add(new JLabel("Active threads", JLabel.CENTER));
        nThreadsLabel = new JLabel("0", JLabel.CENTER);
        interior.add(nThreadsLabel);
        Dimension dim = mainWindow.getToolkit().getScreenSize();
        Rectangle abounds = mainWindow.getBounds();
        mainWindow.setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);
        mainWindow.setVisible(true);
        refreshView();

        for (; ; ) {
            Socket sock = ssock.accept();
            new Thread(new Server(sock)).start();
        }
    }

    private synchronized static void refreshView() {
        nThreadsLabel.setText("" + serverPool.size());
        nRegisteredUsers.setText("" + User.getUsersCount());
    }

    @Override
    public void run() {
        serverPool.add(this);
        refreshView();
        try {
            out = new PrintWriter(sock.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            mainLoop:
            for (; ; ) {
                String s = null;
                try {
                    s = in.readLine();
                } catch (SocketException e) {
                    break;
                }
                if (s == null) break;
            /*
                interpretation of a command/data sent from clients
            */
                if (s.charAt(0) == '/') {
                    // out.println("You entered a command " + s);
                    StringTokenizer st = new StringTokenizer(s);
                    String cmd = st.nextToken();
                    switch (cmd) {
                        case "/register":
                            try {
                                String email = st.nextToken(), password = st.nextToken(), nick = st.nextToken();
                                User u = new User(email, password, nick);
                                if(u.getId() > 0) {
                                    out.println("The user was successfully registered with id " + u.getId());
                                    refreshView();
                                } else {
                                    out.println("The user cannot be registered");
                                }
                            } catch(NoSuchElementException ex) {
                                out.println("Not enough parameters");
                            }
                            break;
                        case "/login":
                            if(st.hasMoreTokens()) {
                                login = st.nextToken();
                                out.println("Welcome on the board, " + login);
                            } else {
                                login = null;
                                out.println("Bye bye");
                            }
                            break;
                        case "/to":
                            sendTo.clear();
                            while(st.hasMoreTokens()) {
                                sendTo.add(st.nextToken());
                            }
                            out.println("Next messages will be sent to " + sendTo);
                            break;
                        case "/whoami":
                            int n = 0;
                            for(Server server: serverPool) {
                                if(server == this)
                                    out.print("*");
                                else
                                    out.print(" ");
                                out.println(server.login != null ? server.login : "not-logged-in");
                                if(server.login != null) n++;
                            }
                            out.println("Clients: " + serverPool.size() + ", logged-in: " + n);
                            break;
                        case "/exit":
                            sock.close();
                            break mainLoop;
                        default:
                            out.println("Unknown command " + cmd);
                    }
                } else {
                    if (login != null) {
                        int n = 0;
                        for(Server server: serverPool) {
                            if(server != this && server.login != null && (sendTo.contains("*") || sendTo.contains(server.login))) {
                                server.out.println(login + " >> " + s);
                                out.println(server.login + " << " + s);
                                n++;
                            }
                        }
                        out.println("Message sent to " + n + " clients");
                    } else {
                        out.println("You have to log in first");
                    }
                }
            }
        } catch (IOException e) {
        }
        serverPool.remove(this);
        refreshView();
    }
}
