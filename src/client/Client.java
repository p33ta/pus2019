/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import java.util.*;

/**
 * @author Mariusz
 */
public class Client extends JFrame implements ActionListener, KeyListener, WindowListener, Runnable {

    private final JTextField tf;
    private final JScrollPane skr;
    private final JTextArea panelg;
    private final JButton bok;
    private PrintWriter out = null;
    private BufferedReader in = null;

    private InetAddress addr;
    private int port;
    private boolean reconnect;

    public Client(String title) {
        super(title);
        setSize(500, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container interior = getContentPane();
        interior.setLayout(new BorderLayout());
        panelg = new JTextArea();
        panelg.setEditable(false);
        skr = new JScrollPane(panelg);
        interior.add(skr, BorderLayout.CENTER);
        JPanel paneld = new JPanel();
        paneld.setLayout(new BorderLayout());
        tf = new JTextField();
        paneld.add(tf, BorderLayout.CENTER);
        bok = new JButton("OK");
        bok.addActionListener(this);
        tf.addKeyListener(this);
        paneld.add(bok, BorderLayout.EAST);
        interior.add(paneld, BorderLayout.SOUTH);
        addWindowListener(this);
        Dimension dim = getToolkit().getScreenSize();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);
    }

    public void connect() throws IOException {
        // defaults
        addr = InetAddress.getByName("localhost");
        port = 8888;
        reconnect = false;

        // properties overloading
        Properties props = new Properties();
        props.load(new FileInputStream("Client.properties"));
        addr = InetAddress.getByName(props.getProperty("host"));
        port = Integer.parseInt(props.getProperty("port"));
        reconnect = Boolean.parseBoolean(props.getProperty("reconnect"));
        String connectTo = addr.getHostAddress() + ":" + port;
        Socket sock = new Socket(addr.getHostName(), port);
        setTitle("Connected to " + connectTo);
        out = new PrintWriter(sock.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    }

    public static void main(String[] args) {
        Client f = new Client("Communicator client");

        try {
            f.connect();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            System.exit(1);
        }

        new Thread(f).start();
        f.setVisible(true);
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            bok.doClick();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
        tf.requestFocus();
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        String s = tf.getText();
        if (s.equals("")) return;
        try {
            if(out != null) {
                out.println(s);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            System.exit(0);
        }
        tf.setText(null);
    }

    @Override
    public void run() {
        for (; ; ) {
            try {
                String s = (in != null) ? in.readLine() : null;
                if (s == null) {
                    if(reconnect) {
                        try {
                            in = null;
                            out = null;
                            setTitle("Trying to reconnect...");
                            Thread.sleep(1000);
                            connect();
                        } catch(Exception e) {}
                    } else {
                        JOptionPane.showMessageDialog(null, "Connection closed by the server");
                        System.exit(0);
                    }
                }
                if(s != null) {
                    panelg.append(s + "\n");
                    skr.getVerticalScrollBar().setValue(skr.getVerticalScrollBar().getMaximum());
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
                System.exit(0);
            }
        }
    }
}